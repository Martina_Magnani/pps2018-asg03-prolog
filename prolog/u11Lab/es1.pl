% dropAny(?Elem,?List,?OutList)
% Drop any occurrence of element

dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]):- dropAny(X,Xs,L).

% Query
% dropAny(10,[10,20,10,30,10],L).

% ----------------------------------------------------------------
% dropFirst(?Elem,?List,?OutList)
% Drops only the first occurrence (showing no alternative results)

dropFirst(X,[X|T],T):- !.
dropFirst(X,[H|Xs],[H|L]):- dropFirst(X,Xs,L).

% Query
% dropFirst(10,[10,20,10,30,10],L).

% ----------------------------------------------------------------
% dropLast(?Elem,?List,?OutList)
% Drops only the last occurrence (showing no alternative results)

dropLast(X,L,LO):- reverse(L, LR),
									 dropFirst(X, LR, LOR),
									 reverse(LOR, LO).
% Query
% dropLast(10,[10,20,10,30,10],L).

% ----------------------------------------------------------------
% dropAll(?Elem,?List,?OutList)
% Drop all occurrences, returning a single list as result

dropAll(X,[],[]).
dropAll(X, [X|Xs], L):- !,
				dropAll(X, Xs, L).
dropAll(X, [H|Xs], [H|L]):- dropAll(X,Xs,L).

% Query
% dropAll(10,[10,20,10,30,10],L).
