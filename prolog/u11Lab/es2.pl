% fromList(+List,-Graph)
% Obtain a graph from a list

fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]):- fromList([H2|T],L).

% Query
% fromList([10,20,30],[e(10,20),e(20,30)]).
% fromList([10,20],[e(10,20)]).
% fromList([10],[]).


% ----------------------------------------------------
% fromCircList(+List,-Graph)
% Obtain a graph from a circular list

fromCircList([H|T], G):- fromCircList([H|T], G, H).
fromCircList([X], [e(X,H)], H).
fromCircList([H1,H2|T], [e(H1,H2)|L], H):- fromCircList([H2|T], L, H).

% Query
% fromCircList([10,20,30],[e(10,20),e(20,30),e(30,10)]).
% fromCircList([10,20],[e(10,20),e(20,10)]).
% fromCircList([10],[e(10,10)]).


% ----------------------------------------------------
% dropNode(+Graph, +Node, -OutGraph)
% drop all edges starting and leaving from a Node
% use dropAll defined in 1.1

% dropAll(?Elem,?List,?OutList)
dropAll(X,[],[]).
dropAll(X, [e(X,_)|Xs], L):- !, dropAll(X, Xs, L).
dropAll(X, [e(_,X)|Xs], L):- !, dropAll(X, Xs, L).
dropAll(X, [H|Xs], [H|L]):- dropAll(X,Xs,L).


dropNode(G, N, O):- dropAll(N, G, O).

% Query
% dropNode([e(1,2),e(1,3),e(2,3)],1,[e(2,3)]).


% ----------------------------------------------------
% reaching(+Graph, +Node, -List)
% all the nodes that can be reached in 1 step from Node
% possibly use findall, looking for e(Node,_) combined
% with member(?Elem,?List)

reaching([], N, []).
reaching([e(N,Y)|Xs], N, [Y|O]):- !, reaching(Xs, N, O).
reaching([e(X,_)|Xs], N, O):- reaching(Xs, N, O).

% Query
% reaching([e(1,2),e(1,3),e(2,3)],1,L). -> L/[2,3]
% reaching([e(1,2),e(1,2),e(2,3)],1,L). -> L/[2,2]).


% ----------------------------------------------------
% anypath(+Graph, +Node1, +Node2, -ListPath)
% a path from Node1 to Node2
% if there are many path, they are showed 1-by-1

%anypath([], N1, N2, []).
%anypath([e(N1,N2)|Xs], N1, N2, [e(N1,N2)|O]):- anypath(Xs, N1, N2, O).
%anypath([e(N1,X)|Xs], N1, N2, O):-	anypath(Xs, X, N2, O).

anypath([e(N1, N2)|Xs], N1, N2, [e(N1, N2)]).
anypath([_|T], N1, N2, L):- anypath(T, N1, N2, L).	
anypath([e(N1,X)|T], N1, N2,[e(N1,H)|P]):-
			reaching([e(N1,X)|T], N1, [H|L]),
			anypath([e(N1,X)|T], H, N2, P).

% Query
% anypath([e(1,2),e(1,3),e(2,3)],1,3,L).


% ----------------------------------------------------
% allreaching(+Graph, +Node, -List)
% all the nodes that can be reached from Node
% Suppose the graph is NOT circular!
% Use findall and anyPath!

allreaching(G,N,L):- findall(X, anypath(G, N, X, _), L).

% Query
% allreaching([e(1,2),e(2,3),e(3,5)],1,[2,3,5]).