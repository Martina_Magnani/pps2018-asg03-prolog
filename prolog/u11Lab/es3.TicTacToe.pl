% Tic Tac Toes

% next(@Table,@Player,-Result,-NewTable)

% Table � una rappresentazione di una tabella TTT
% in cui giocano i giocatori x e o.
% Player (x oppure o) � il giocatore che muove.
% Result pu� essere:
% vittoria (win(x), win(o)), niente (nothing), o pareggio (even).
% NewTable � la nuova tabella dopo una mossa valida.

% Bisogna trovare una rappresentazione della Table.
% Chiamare questo predicato dovrebbe produrre tutti i risultati validi.

% show(+Board)
% Show the board to current output.
show([X1, X2, X3, X4, X5, X6, X7, X8, X9]) :-
    write('   '), show2(X1),
    write(' | '), show2(X2),
    write(' | '), show2(X3), nl,
    write('  --------------  '), nl,
    write('   '), show2(X4),
    write(' | '), show2(X5),
    write(' | '), show2(X6), nl,
    write('  --------------  '), nl,
    write('   '), show2(X7),
    write(' | '), show2(X8),
    write(' | '), show2(X9), nl.

% show2(+Term)
% Write the term to current outupt
% Replace n by ' '.
show2(X) :-
    X == n,
    write(' '), !.

show2(X) :-
    write(X).

isWinPos(P, [X11,X12,X13,X21,X22,X23,X31,X32,X33]):-
			equal(X11,X12,X13,P); %1st line
			equal(X21,X22,X23,P); %2nd line
			equal(X31,X32,X33,P); %3rd line
			equal(X11,X21,X31,P); %1st col
			equal(X12,X22,X32,P); %2nd col
			equal(X13,X23,X33,P); %3rd col
			equal(X11,X22,X33,P); %1st diag
			equal(X13,X22,X31,P). %2nd diag

equal(X, X, X, X).

nextPlayer(o, x).
nextPlayer(x, o).

isEven(T, nothing):- member(n, T), !.
isEven(T, even):- not member(n, T).

%setResult(P, NT, win(P)):- isWinPos(P, NT), !.
%setResult(P, NT, R):- isWinPos(P, NT), newPlayer(P, NP), not isWinPos(NP, NT); isEven(NT, R).

setResult(P, NT, R):-
			(isWinPos(P, NT)
			-> R = win(P) 
				  % write(P), write('  '), write(NT), write('  '), write(R), nl)
			; isEven(NT, R)
				 % write(P), write('  '), write(NT), write('  '), write(R), nl)
			).

move(P, [n|Ts], [P|Ts]).
move(P, [T|Ts], [T|T2s]):-
		move(P, Ts, T2s).

next(T, P, R, NT):-
			move(P, T, NT),
			setResult(P, NT, R).
			
% Query
% next([n,n,n,x,o,x,o,x,n],o,R,NT).


% ------------------------------------------------
% game(@Table,@Player,-Result,-TableList)
% TableList � la sequenza di tabelle fino al Result win(x), win(o) oppure even.

game(T, P, win(X), [T|[NT]]):- 
					next(T, P, win(X), NT),
					show(NT),
					write('Win! Player '),
					write(win(X)),
					write(' has won!'), nl.
					
game(T, P, even, [T|[NT]]):-
					next(T, P, even, NT),
					show(NT),
					write('Draw! The game is over without winners!'), nl.			

game(T, P, R, [T|TL]):- 
		next(T, P, nothing, NT),
		nextPlayer(P, NP),
		game(NT, NP, R, TL).
				
% Query
% game([n,n,n,x,o,x,o,x,n], o, R, TL).