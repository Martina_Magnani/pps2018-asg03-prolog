% size(List,Size)
% Size will contain the number of elements in List

size([],0).
size([_|T],M) :- size(T,N), M is N+1.

% Query
% size( [a,b,c], X ). -> X/3

% size2(List,Size)
% Size will contain the number of elements in List, written using notation zero, s(zero), s(s(zero))..

size2([], zero).
size2([_|T], s(Y)) :- size2(T, Y).

% Query
% size2( [a,b,c], X ). -> X/s(s(s(zero)))
% size2(L, s(s(s(zero)))). -> L/[_2745,_2747,_2749]