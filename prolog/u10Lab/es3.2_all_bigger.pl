% all_bigger(List1,List2)
% all elements in List1 are bigger than those in List2, 1 by 1


isBigger(X,Y):- X > Y.

all_bigger([],[]).
all_bigger([X|Xs], [Y|Ys]):-
		isBigger(X,Y),
		all_bigger(Xs,Ys).


% Query
% all_bigger([10,20,30,40],[9,19,29,39]). --> yes
% example: all_bigger([8,20,30,40],[9,19,29,39]). --> no
% example: all_bigger([9,20,30,40],[9,19,29,39]). --> no