% seqR2(N,List)
% Note, you may need to add a predicate �last�
% last([1,2,3],5,[1,2,3,5]).

seqR2(N,List):-seqR2(0,N,List).
seqR2(Last, Last, [Last]).
seqR2(N,Last,[X|Xs]):-
			N == X,
			N2 is N+1,
			seqR2(N2, Last, Xs).
			

% Query
% seqR2(4,[0,1,2,3,4]).