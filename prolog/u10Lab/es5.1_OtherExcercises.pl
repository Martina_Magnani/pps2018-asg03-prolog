% Other Exercises

% --------------------------------------
% inv(List,List)
        
inv(List, Rev):-
		inv(List, Rev, []).
		
inv([], L, L).
inv([H|T], L, SoFar):-
		inv(T, L, [H|SoFar]).

% Query
% inv([1,2,3],[3,2,1]).


% --------------------------------------
% double(List,List)
% suggestion: remember predicate append/3

% append --> concatenates 1st and 2nd list, with output in 3rd  
append([], L, L).
append([H|T], L, [H|M]):- append(T, L, M).

double([], L, L).
double(List, Double):-
		append(List, List, ListList),
		double([], Double, ListList).
		
% Query
% double([1,2,3],[1,2,3,1,2,3]).


% --------------------------------------
% times(List,N,List)

times(List, C, NTimes):-
			times(List, C, NTimes, List).
		
times(List, 0, [], List).
times([], C, Xs, List):-
			C2 is C-1,
			times(List, C2, Xs, List).
times([X|Xs], C, [X|Ys], List):-
			C > 0,
			times(Xs, C, Ys, List).
		
% Query
% times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).


% --------------------------------------
% proj(List,List)
proj([], []).
proj([[X|_]|Xs],[X|Ys]):-
		proj(Xs,Ys).

% Query
% proj([[1,2],[3,4],[5,6]],[1,3,5]).