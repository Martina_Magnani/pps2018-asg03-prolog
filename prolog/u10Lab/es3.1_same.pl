% same (List1, List2)
% are the two lists the same?

same([],[]).
same([X|Xs],[X|Ys]):-same(Xs,Ys).

% Query
% same([3,4,3], [3,3,3]). --> no
% same([3,4,3], [3,4,3]). --> yes 