% sum(List,Sum)

sum([],0).
sum([H|T], M) :- sum(T,N), M is N+H.

% Query
% sum([1,2,3],X).
% X/6