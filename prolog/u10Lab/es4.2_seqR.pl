% seqR(N,List)

seqR(0,[0]).
seqR(N, [N|Xs]):-
		N2 is N-1,
		seqR(N2,Xs).

% Query
% seqR(4,[4,3,2,1,0]). --> yes
% seqR(4,[4,3,2,1]). --> no
% seqR(3,[4,3,2,1,0]). --> no