% max(List,Max)
% Max is the biggest element in List
% Suppose the list has at least one element

isTempMax(Elem,TempMax,Elem) :- TempMax =< Elem.
isTempMax(Elem,TempMax,TempMax) :- TempMax > Elem.

max(List,Max) :- max(List,Max,TempMax).
max([],Max,TempMax) :- Max is TempMax.
max([X|Xs],Max,TempMax) :-
	isTempMax(X, TempMax, Y),
	max(Xs,Max,Y).

	
 