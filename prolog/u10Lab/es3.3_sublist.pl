% sublist(List1,List2)
% List1 should be a subset of List2

search(X,[X|_]).
search(X,[_|Xs]):-search(X,Xs).

sublist([], _).
sublist([X|Xs], Ys):-
		search(X, Ys),
		sublist(Xs,Ys).

% Query
% sublist([1,2],[5,3,2,1]).

