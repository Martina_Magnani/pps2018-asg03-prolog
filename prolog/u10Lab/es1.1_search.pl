% search(Elem,List)
% Leggi il codice come segue:
% - la ricerca è OK se l'elemento X è la testa della lista
%- la ricerca è OK se l'elemento X è presente nella coda Xs

search(X,[X|_]).
search(X,[_|Xs]):-search(X,Xs).

% Più scopi:
%  query:
%   search(a,[a,b,c]).
%   search(a,[c,d,e]).
%  iteration:
%   search(X,[a,b,c]).
%  generation:
%   search(a,X).
%   search(a,[X,b,Y,Z]).
%   search(X,Y).