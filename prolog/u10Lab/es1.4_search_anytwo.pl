% search_anytwo(Elem,List)
% looks for any Elem that occurs two times

search(X,[X|_]).
search(X,[_|Xs]):-search(X,Xs).

search_anytwo(X,[X|Xs]):-search(X,Xs).
search_anytwo(X,[_|Xs]):-search_anytwo(X,Xs).

%Query:
% � search_anytwo(a,[b,c,a,a,d,e]).  - yes
% � search_anytwo(a,[b,c,a,d,e,a,d,e]). - yes

